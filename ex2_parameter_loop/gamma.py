# Code modified from the following sources:
# https://stackoverflow.com/questions/42150965/how-to-plot-gamma-distribution-with-alpha-and-beta-parameters-in-python
# https://stackoverflow.com/questions/9622163/save-plot-to-image-file-instead-of-displaying-it-using-matplotlib

import numpy as np
import scipy.stats as stats 
from matplotlib import pyplot as plt
import sys

alpha = int(sys.argv[1])
beta = int(sys.argv[2])
theta = 1/beta

x = np.linspace (0, 100, 200) 
y1 = stats.gamma.pdf(x, a=alpha, scale=theta)
plt.plot(x, y1, "y-", label=(r"$\alpha={}, \beta={}$".format(alpha,beta))) 

height = beta/alpha
plt.ylim([0,height])
plt.xlim([0,20])

fig = "gamma_a{}_b{}.png".format(alpha, beta)

plt.legend()
plt.savefig(fig)
plt.close()
