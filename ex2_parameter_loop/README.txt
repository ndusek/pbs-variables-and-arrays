This example shows how to submit jobs for a range of parameters (i.e. a parameter sweep) using PBS variables.

There are 3 components to this example:
- gamma.py	  a python script that takes ALPHA and BETA parameters and uses them to plot a gamma distribution (.png)
- job.pbs	  the PBS job script
- submit.sh	  a wrapper script that submits our jobs for us based on a nested loop

Before running: Make sure you change the group_list variable to a group that you are a member of.

To run: ./submit.sh

Expected output: A .png image of a gamma distribution and a *.o[job ID] out file for each of 25 jobs.
