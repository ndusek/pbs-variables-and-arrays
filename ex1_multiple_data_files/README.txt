This example illustrates how to submit a job for each data file in a directory using only one PBS job script and PBS variable lists.

There are 4 components to this example:
- data_*.csv	  files containing pairs of randomly generated (x,y) coordinates
- job.pbs	  the PBS job script
- regression.py	  a python script that reads in a CSV file of (x,y) coordinates and fits them to a linear regression
- submit.sh	  a wrapper script that submits our jobs for us based on a loop

Before running: Make sure you change the group_list variable to a group that you are a member of.

To run: ./submit.sh

Expected output: A *.o[job ID] file with linear regression data for each data*.csv file in the directory.
