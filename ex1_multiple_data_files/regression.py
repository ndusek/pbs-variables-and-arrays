import argparse
import csv
from scipy import stats

parser = argparse.ArgumentParser(description='Performs linear regression on a CSV file of ordered pairs.')
parser.add_argument('file', nargs=1)

x = []
y = []
with open(parser.parse_args().file[0]) as file:
    csvReader =  csv.reader(file, delimiter=',')
    line = 0
    for row in csvReader:
        if line == 0:
            pass
        else:
            x.append(int(row[0]))

            y.append(int(row[1]))
        line += 1

slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

print('    Slope: ' + str(slope))
print('Intercept: ' + str(intercept))
print('       R2: ' + str(r_value ** 2))
    