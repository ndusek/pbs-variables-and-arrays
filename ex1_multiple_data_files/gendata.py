import random

def createCSV(filename):
    file = open(filename, 'w')
    file.write('x,y\n')
    for i in range(1, 101):
        file.write(str(random.randint(0, 100)) + ',' + str(random.randint(0, 100)) + '\n')

for i in range(1, 21):
    createCSV('data_'+str(random.randint(10000, 99999))+'.csv')
