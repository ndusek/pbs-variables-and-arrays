This example shows how to use a PBS job array to submit one job for each line in a data file. The data file may contain parameter lists, filenames, or actual data entries; but the assumption is that each line in the file is independent of the others.

There are 3 components to this example:
- gamma.py	  a python script that takes ALPHA and BETA parameters and uses them to plot a gamma distribution (.png)
- job.pbs	  the PBS job script
- params.csv	  a CSV file containing pairs of ALPHA/BETA parameters.

Before running: Make sure you change the group_list variable to a group that you are a member of.

To run: qsub job.pbs

Expected output: A .png image of a gamma distribution and a *.o[job ID] out file for each of 25 jobs.
